from django.db import models

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=255)
    attachment = models.FileField(upload_to='files')

    def __str__(self):
        return self.name